# ellipse-carousel

#### 介绍
旋转木马，3d无限旋转

![alt](/src/assets/screenshot.png)

#### 安装教程
```
npm install ellipse-carousel
```

#### 使用说明
##### 全局安装
```
import Vue from 'vue';
import EllipseCarousel from 'ellipse-carousel';
import 'ellipse-carousel/dist/ellipse-carousel.css';

Vue.use(EllipseCarousel);
```

##### 组件安装
```
import 'ellipse-carousel/dist/ellipse-carousel.css';
import { EllipseCarousel, EllipseCarouselItem } from 'ellipse-carousel';

export default {
    ...
    components: {
        EllipseCarousel,
        EllipseCarouselItem
    }
    ...
};
```

#### API
##### Carousel props
| 属性 | 说明 | 类型 | 默认值 |
| :-----| ---- | :----: | :----: |
| height | 高度 | [String, Number] | auto |
| autoplay | 是否自动播放 | Boolean  | false |
| autoplaySpeed | 自动切换的时间间隔，单位为毫秒 | Number  | 5000 |
| arrow | 切换箭头的显示时机，可选值为 hover（悬停），always（一直显示），never（不显示） | String  | hover |
| elementclick | item元素是否可以点击切换 | Boolean  | true |



##### Carousel events

| 事件名 | 说明 | 返回值 |
| :----- | ---- | :----: |
| on-change | 点击幻灯片时触发，返回索引值 | index |



