const packageInfo = require('./package.json');
const GenerateJsonPlugin = require('generate-json-webpack-plugin');
module.exports = {
    outputDir:'example',
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
    },
    productionSourceMap:false,
    lintOnSave:false,
    configureWebpack: config => {
        
        config.output.libraryExport = 'default'

        if(process.env.MODE === 'production'){
            config.plugins.push(
                new GenerateJsonPlugin('package.json', {
                    "name": packageInfo.name,
                    "version": packageInfo.version,
                    "description": packageInfo.description,
                    "author": packageInfo.author || 'bingo',
                    "main": "./ellipse-carousel.umd.js",
                    "peerDependencies": {},
                    "dependencies": {},
                    "license": "MIT",
                    "engines": {
                        "node": ">= 4.0.0",
                        "npm": ">= 3.0.0"
                    }
                })
            )
        }
       
    }
}