import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import vuecarousel from '../dist/ellipse-carousel.umd'
import '../dist/ellipse-carousel.css'
Vue.use(vuecarousel);

new Vue({
  render: h => h(App),
}).$mount('#app')


