import EllipseCarouselItem from '../ellipse-carousel/ellipse-carousel-item';

EllipseCarouselItem.install = Vue => Vue.component(EllipseCarouselItem.name, EllipseCarouselItem);

export default EllipseCarouselItem;
