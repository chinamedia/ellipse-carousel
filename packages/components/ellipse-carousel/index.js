import EllipseCarousel from './ellipse-carousel';

EllipseCarousel.install = Vue => Vue.component(EllipseCarousel.name, EllipseCarousel);

export default EllipseCarousel;
