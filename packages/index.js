import packageConfig from '../package.json';
// 组件
import EllipseCarousel from './components/ellipse-carousel';
import EllipseCarouselItem from './components/ellipse-carousel-item';

const components = {
    EllipseCarousel,
    EllipseCarouselItem
};

const install = function(Vue) {
    if (install.installed) return; 

    Object.keys(components).forEach(key => {
        Vue.component(key, components[key]);
    });
};

// auto install
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

const version = packageConfig.version;

const  API = {
    version,
    install,
    ...components
};

export { 
    version,
    install,
    EllipseCarousel, 
    EllipseCarouselItem 
};
export default API ;


